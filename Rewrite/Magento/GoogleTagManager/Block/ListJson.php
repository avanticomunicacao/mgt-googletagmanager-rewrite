<?php
declare(strict_types=1);

namespace Avanti\GoogleTagManagerRewrite\Rewrite\Magento\GoogleTagManager\Block;

class ListJson extends \Magento\GoogleTagManager\Block\ListJson
{
    protected function _getProductCollection()
    {
        if(!$this->getListBlock()) {
            return null;
        }

        if ($this->_productCollection === null) {
            $this->_productCollection = $this->getListBlock()->getLoadedProductCollection();
        }

        if ($this->_showCrossSells && (null === $this->_productCollection)) {
            $this->_productCollection = $this->getListBlock()->getItemCollection();
        }

        if ((null === $this->_productCollection)
            && ($this->getBlockName() == 'catalog.product.related'
                || $this->getBlockName() == 'checkout.cart.crosssell')
        ) {
            $this->_productCollection = $this->getListBlock()->getItems();
        }

        return $this->_productCollection;
    }
}