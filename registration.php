<?php

use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(
    ComponentRegistrar::MODULE,
    'Avanti_GoogleTagManagerRewrite',
    __DIR__
);